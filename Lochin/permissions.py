from rest_framework.permissions import BasePermission, SAFE_METHODS
import datetime


class IsAdminOrUser(BasePermission):

    def has_permission(self, request, view):
        if request.method in ['POST', 'PUT', 'PATCH']:
            user = getattr(request, 'user', None)
            if user.is_staff == True:
                return True

            return bool(request.user)
        return True

    def has_object_permission(self, request, view, obj):

        if request.method in ['PUT', 'PATCH']:
            if request.user == obj.user or request.user.is_staff == True:
                return True
            else:
                return False
        return True
