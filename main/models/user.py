from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import PROTECT



class User(AbstractUser):

    client = models.OneToOneField('main.Client', PROTECT, related_name='user', null=True)
    full_name = models.CharField(max_length=500, null=True)

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username


    def __str__(self):
        return self.username

    # @property
    # def person(self):
    #     if not bool(self.user_type):
    #         return None, self
    #     if self.user_type != self.ADMIN:
    #         return self.user_type, getattr(self, self.user_type)
    #     return self.user_type, self

    # class Meta:
    #     unique_together = ['client']