from django.db import models

class Client(models.Model):
    in_blacklist = models.BooleanField(default=False)
    full_name = models.CharField(max_length=255, null=True)
    phone_number = models.CharField(max_length=255, null=True)
    is_mailing = models.BooleanField(default=True)

    class Meta:
        ordering = ['-id']