from django.db import models

class Product(models.Model):
    name = models.CharField('Название продукта', max_length=50)
    description = models.TextField("Описание")
    url = models.SlugField(max_length=160, unique=True)
    country = models.CharField("Страна", max_length=30)
    brand = models.CharField("Бренд", max_length=100)
    package = models.CharField("Упаковка", max_length=100)
    type = models.CharField("Тип", max_length=100)
    price = models.PositiveIntegerField("Цена")
    vendor_code = models.PositiveIntegerField("Артикул")
    # image = models.ImageField("Фото", upload_to="productsPhoto/", null=True)

    category = models.ForeignKey('main.Category', on_delete=models.SET_NULL, null=True, related_name="categories")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"