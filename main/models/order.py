from django.db import models

class Order(models.Model):
    totalPriceAmount = models.PositiveIntegerField("Общая цена")

    # orderType = models.ForeignKey(OrderType, on_delete=models.CASCADE, related_name='orderTypes')

    user = models.ForeignKey('main.User', on_delete=models.CASCADE, related_name='users')

    def __str__(self):
        # return str(self.id)
        return self.user.name
    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"


class Orderitem(models.Model):
    productQuantaty = models.PositiveIntegerField("Количество продуктов")
    price = models.PositiveIntegerField("Цена")

    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='orders')
    product = models.ForeignKey('main.Product', on_delete=models.CASCADE, related_name='products')

    def __str__(self):
        return str(self.id)