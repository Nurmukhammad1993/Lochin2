from rest_framework import serializers
from main.models import Product

class ProductListSerializer(serializers.ModelSerializer):
    category = serializers.SlugField('name', read_only=True)

    class Meta:
        model = Product
        fields = "__all__"