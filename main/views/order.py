from django.db import models

from rest_framework import viewsets
from main.serializers import OrderCreateSerializer
from main.models import Order
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from Lochin.permissions import IsAdminOrUser
from django.db.models import Q
import datetime


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderCreateSerializer

    permission_classes = (IsAdminOrUser, IsAuthenticated)

    authentication_classes = (TokenAuthentication, SessionAuthentication)

    #
    # def get_queryset(self):
    #     qs = Order.objects.all()
    #     # print(qs, type(qs))
    #     if self.action in ['list', 'retrieve'] and self.request.user.is_staff == False:
    #         qs = qs.filter(user=self.request.user)
    #     return qs


