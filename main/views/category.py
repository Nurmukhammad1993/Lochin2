from main.models.category import Category
from rest_framework import viewsets
from main.serializers import CategoryListSerializer
from rest_framework.authtoken.serializers import AuthTokenSerializer

class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategoryListSerializer