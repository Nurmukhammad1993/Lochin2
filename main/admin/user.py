from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from main.models import User


class User_Admin(UserAdmin):
    list_display = ('id', 'username', 'email')


admin.site.register(User, User_Admin)
