from django.contrib import admin

from main.models import Client

@admin.register(Client)
class Admin(admin.ModelAdmin):
    list_display = ['id', 'full_name']