from django.contrib import admin

from main.models import Orderitem, Order

@admin.register(Orderitem)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ('id','order', 'product')


admin.site.register(Order)