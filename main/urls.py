from django.urls import path, include
from rest_framework.routers import DefaultRouter


from main.views import ProductViewSet, OrderViewSet, CategoryViewSet

router = DefaultRouter()
router.register('product', ProductViewSet, basename='product')
router.register('category', CategoryViewSet, basename='category')
router.register('order', OrderViewSet, basename='order')


urlpatterns = [
    path('', include(router.urls))
]